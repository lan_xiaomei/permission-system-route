import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // 设置 page title
  document.title = getPageTitle(to.meta.title)

  // // 判断用户是否登录，登录了是否有用户信息，有用户信息是否有权限访问跳转的路径
  // const hasToken = getToken()//获取token
  // if (hasToken) {//判断是否登录（token）
  //   if (to.path === '/login') {//to.path表示要去的路径
  //     // 登录后，试图重定向到登录页面的，重新跳转到应用首页
  //     next({ path: '/' })//回到首页（roouter中定义）
  //     NProgress.done()
  //   } else {
  //     const hasGetUserInfo = store.getters.name
  //     if (hasGetUserInfo) {//判断是否有用户权限
  //       next()
  //     } else {
  //       try {
  //         // get user info
  //         await store.dispatch('user/getInfo')
  //         next()
  //       } catch (error) {
  //         // remove token and go to login page to re-login
  //         await store.dispatch('user/resetToken')
  //         Message.error(error || 'Has Error')
  //         next(`/login?redirect=${to.path}`)
  //         NProgress.done()
  //       }
  //     
  //   }
  // } else {
  //   /* 没有登录（has no token）*/

  //   if (whiteList.indexOf(to.path) !== -1) {
  //     // in the free login whitelist, go directly
  //     next()
  //   } else {
  //     //其他没有访问权限的页面是redirec
  //     next(`/login?redirect=${to.path}`)
  //     NProgress.done()
  //   }
  // }

const hasToken = token  (获取令牌，得到用户信息)
if(hasToken){//如果获取令牌
  if(登陆成功){
     跳转页面，前往主页
}else{登陆失败
  获取用户名
if（userInfo）{用户信息存在
  前往下一级路由
}else{登陆成功，但尚未填写用户信息
        去获取用户信息().then(()=>{
         前往下一级路由
      }).catch(()=>{ 
       重置令牌
       跳转到下一级路由
}
}
}

}else{令牌获取失败
  if（判断是否为白名单）{
   直接跳转到默认的主页
}else{
  跳转到登陆路径
 

}
}

})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
