import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {                    //路由跳转
  // start progress bar
  NProgress.start()                                             //开始进度条，启动

  // set page title
  document.title = getPageTitle(to.meta.title)                  //获取页面信息

  // determine whether the user has logged in
  const hasToken = getToken()                                   //获取临时令牌,即用户信息

  if (hasToken) {                                               //验证用户信息
    if (to.path === '/login') {                                 //验证路径是否为登录页面路径
      // if is logged in, redirect to the home page
      next({ path: '/' })                                       //如果已经登录，则重新定向主页    //主页在router中定义
      NProgress.done()                                          //进度条结束
    } else {
      const hasGetUserInfo = store.getters.name                 //传递用户信息
      if (hasGetUserInfo) {                                     //判断是否传递用户信息
        next()                                                  //重新定义(跳转)页面
      } else {
        try {                                                   //try可以继续运行防止判断错误时报错
          // get user info
          await store.dispatch('user/getInfo')                  //await只能在异步方法下使用 //把await及获取它的值的操作放在try里

          next()                                                //重新定义(跳转)页面
        } catch (error) {                                       
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')               //失败的操作放在catch里
          Message.error(error || 'Has Error')                   //输出错误信息
          next(`/login?redirect=${to.path}`)                    //重新定义(跳转)页面,跳转登录页面
          NProgress.done()                                      //页面加载进度条
        }
      }
    }
  } else {
    //没有获取令牌时
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {                    //判断是否白名单
      // in the free login whitelist, go directly
      next()                                                    //重新定义(跳转)页面
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)                        //重新定义(跳转)页面,跳转登录页面
      NProgress.done()                                          //页面加载进度条
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})

//token是服务端生成的一串字符串，代表向服务端发出临时请求(临时也可以是长期)，类似于记住账号密码，方便下一次登录直接发送请求

//token可以减轻服务器压力，减少访问数据库的次数

//try{}catch{}：运行错误时,如果没有try的话，程序直接崩溃。用try的话，则可以让程序运行下去，并且用catch输出为什么出错

//路由实例.钩子函数(异步方法(跳转前页面,跳转后页面，跳转指令)=> {
  //开始进度条,启动                                           
  //文档.字符串 = 获取页面标题(当前页面,提供关于HTML文档的元数据,标题)                 
  //常量 字符串 = 获取令牌                                  
  //if (是否获取令牌) {                                              
    //if (验证路径是否为登录页面路径) {                                 
      //跳转({页面})                                       
      //进度条结束                                          
    //} else {
      //常量 字符串 = 获取信息                 
      //if (是否传入信息) {                                     
        //跳转页面
      //} else {
        //try {                                                   
          //用await把await及获取它的值的操作放在try里
          //跳转页面                                                
        //} catch (错误) {                                     
          //用await把失败的操作放在catch里
          //输出错误信息
          //跳转(页面)
          //页面加载进度条                       
        //}
      //}
    //}