import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // 设置文章title
  
  document.title = getPageTitle(to.meta.title)

  // 这里的getToken()就是在上面导入的auth.js里的getToken()方法        
  const hasToken = getToken()
  //如果存在已登陆的令牌
  if (hasToken) {
    //如果用户存在令牌的情况请求登录页面，就让用户直接跳转到首页
    if (to.path === '/login') {
     // 直接跳转到首页 path: '/'
      next({ path: '/' })
      NProgress.done()
    } else {
      //拿到用户的信息
      const hasGetUserInfo = store.getters.name
     
      if (hasGetUserInfo) {
        //用户任意请求
        next()
      } else {
        try {
            // 如果有令牌，但是没有用户信息，证明用户是第一次登录，设置用户信息
          await store.dispatch('user/getInfo')
         // 设置好了之后，可以请求哪就跳转哪
          next()
        } catch (error) {
           // 如果出错了，把令牌去掉，并让用户重新去到登录页面
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
//没令牌
//判断用户请求的路由是否在白名单里
    if (whiteList.indexOf(to.path) !== -1) {
      // 不是-1就证明存在白名单里，不管你有没有令牌，都直接去到白名单路由对应的页面
      next()
    } else {
     // 如果这个页面不在白名单里，直接跳转到登录页面
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  ///每次请求结束后都需要关闭进度条
  NProgress.done()
})


const 有了令牌 = getToken()

if (用户存在令牌的情况请求) {
  
  if (用户请求登录) {
  
    next(让他到首页)
    NProgress.done()
  } else有令牌我不登陆 {
    拿到用户的信息   
    if (可以拿到你的信息) {
    老用户了随便转转
    } else 有令牌但是没有用户信息{
      try {
          说明用户是第一次登录
          设置用户信息();
       设置好了之后可以谁便转转
      } catch ( 如果出错了) {
         把令牌去掉()
         并让用户重新去到登录页面()   
      }
    }
  }
} else {
 //没令牌
  if (判断用户请求的路由是否在白名单里) {
  直接去到白名单专属vip路由页面() 
  } else {
   //如果这个页面不在白名单里
    next(回到登录路由页面)
    NProgress.done()
  }
}

