import router from './router'
import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
// import user from 'mock/user'
// import { login } from './api/user'
import { canTurnTo } from '@/utils/access'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const trunTo = (to, access, next) => {
  var can = canTurnTo(to.name, access, router.options.routes)
  if (can) next() // 拥有权限，可以访问
  else next({ replace: true, name: 'error_401' }) // 没有权限，变到401页面
}

router.beforeEach(async(to, from, next) => {
  NProgress.start()

  // 设置网页的标题
  document.title = getPageTitle(to.meta.title)

  // 判断用户是否登录，登录后是否有用户信息，有了用户信息之后是否有权限访问要跳转的路由
  const hasToken = getToken() // var hasLogin=获取token
  if (hasToken) {
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done
    } else {
      var userInfo = store.state.user.name
      if (userInfo) {
        trunTo(to, store.state.user.access, next)
      } else {
        console.log('登录成功，已经有token，但是还没有用户信息，准备获取用户信息')
        store.dispatch('user/getInfo').then(user => {
          // 获取用户信息，通过用户权限何跳转的页面的name来判断是否有权限访问；
          // access必须是一个数组，如：['super_admin']['super_admin','admin']
          trunTo(to, user.access, next)
        }).catch(() => {
          // 获取用户信息失败，重置token
          store.dispatch('user/resetToken')
          next('/login') // access_token   key value
        })
      }
    }
  } else {
    // 没有登录（因为没有token）
    if (to.path === '/login') {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

// var  hasToken  =  获取令牌()

// if(hasToken){
//   if(登录成功){
//     前往主页
//   }else{
//     var userInfo = 获取用户名()
//     if(userInfo){
//       前往下一级路由
//     }else{
//       获取用户信息().then(()=>{
//         前往下一级路由
//       }).catch(()=>{        
//         刷新令牌信息()
//         回到登录页面
//       })
//     }
//   }
// }else{
//   返回登录页面
// }

router.afterEach(() => {
  NProgress.done
})

