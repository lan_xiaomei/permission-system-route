import router from './router'
import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import { canTurnTo } from '@/utils/access'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

const turnTo = (to, access, next) => {//to:代表本次将要跳转的路由或者说是路径比如/userinfo,from(access):代表本次跳转从哪里来的路由，通俗来说就是从那个路由跳转过来 next：将要跳转的下一级路由
  var can = canTurnTo(将要跳转的路由名字to.name, access, router.options.routes)
  if (can) next() // 有权限，可访问
  else 将要跳转的下一级路由next({ replace: true, path:'/401' }) // 无权限，重定向到401页面
}

路由实例化.拦截路由router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  
  设置网页标题document.title =页面getPageTitle(将要去的路由标题to.meta.title)

  // 判断用户是否登录，登录了是否有用户信息，有用户信息是否有权限访问要跳转的路由
  const hasToken =获取getToken()
  if (hasToken) {
    if (将要去的路径===登录界面to.path === '/login') {
      // 登录后，试图重定向到登录页面的，重新跳转到应用首页
      已登录进入主页面next({ path: '/' })
      运行NProgress.done()
    } else {
     var  userInfo = store.state.user.name
      if (userInfo) {
        turnTo(to,用户权限数组store.state.user.access, next)
      } else {
        console.log('登录成功，已经有token，但是还没有用户信息，准备获用户信息')
        获取用户信息store.dispatch('user/getInfo').then(user => {
          // 拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问;
          // access必须是一个数组，如：['super_admin'] ['super_admin', 'admin']
          turnTo(to, 权限数组user.access, next)
        }).catch(() => {
          // 获取用户信息失败，重置token
          储存store.dispatch('user/resetToken')
          将要去的下一级路由登录界面next('/login')
        })
      }
    }
  } else {
    // 没有登录（因为没有token）

    if (to.path === '/login') {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})


// import router from './router'
// import store from './store'
// // import { Message } from 'element-ui'
// import NProgress from 'nprogress' // progress bar
// import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
// import getPageTitle from '@/utils/get-page-title'
// import { canTurnTo } from '@/utils/access'

// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// // const whiteList = ['/login'] // no redirect whitelist

// const turnTo = (to, access, next) => {
//   var can = canTurnTo(to.name, access, router.options.routes)
//   if (can) next() // 有权限，可访问
//   else next({ replace: true, path:'/401'}) // 无权限，重定向到401页面
// }

// router.beforeEach(async(to, from, next) => {
//   // start progress bar
//   NProgress.start()

//   // 设置网页标题
//   document.title = getPageTitle(to.meta.title)

//   // 判断用户是否登录，登录了是否有用户信息，有用户信息是否有权限访问要跳转的路由
//   const hasToken = getToken()
//   if (hasToken) {
//     if (to.path === '/login') {
//       // 登录后，试图重定向到登录页面的，重新跳转到应用首页
//       next({ path: '/' })
//       NProgress.done()
//     } else {
//       var userInfo = store.state.user.name
//       if (userInfo) {
//         turnTo(to, store.state.user.access, next)
//       } else {
//         console.log('登录成功，已经有token，但是还没有用户信息，准备获用户信息')
//         store.dispatch('user/getInfo').then(user => {
//           // 拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问;
//           // access必须是一个数组，如：['super_admin'] ['super_admin', 'admin']
//           turnTo(to, user.access, next)
//         }).catch(() => {
//           // 获取用户信息失败，重置token
//           store.dispatch('user/resetToken')
//           next('/login')
//         })
//       }
//     }
//   } else {
//     // 没有登录（因为没有token）

//     if (to.path === '/login') {
//       next()
//     } else {
//       next('/login')
//       NProgress.done()
//     }
//   }
// })

// router.afterEach(() => {
//   // finish progress bar
//   NProgress.done()
// })
