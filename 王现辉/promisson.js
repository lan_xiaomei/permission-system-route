import router from './router'
import store from './store'
//import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import { canTurnTo } from '@/utils/access'
import { has } from 'core-js/fn/dict'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

//const whiteList = ['/login'] // no redirect whitelist

const turnTo = (to, access, next) => {
  var can = canTurnTo(to.name, access, router.options.routes)
  if (can) next()
  else next({ replace: true, name: 'error_401' })
}

router.beforeEach(async (to, from, next) => { // 路由实例.路由拦截（异步（将要跳转的路由(路径)，未跳转前的路由(路径)，将要跳转的下一级路由）=>{
  // start progress bar
  NProgress.start()  //跳转进度条.开始（）

  // set page title  设置页面标题
  document.title = getPageTitle(to.meta.title) // 文档.标题 = 获取页面标题（将要去的路由.元数.标题）

 // determine whether the user has logged in
  // 判断用户是否已登录，登录了是否有用户信息，用户信息是否有权限访问要跳转的路由
  const hasToken = getToken()                           // 常量 hasToken（常量名称） = 登录后获取的令牌方法
  if (hasToken) {  // 如果（hasToken（存在））{
    console.log(to.path) //打印出来要跳转的路由的路径
    if (to.path === '/login') {  // 如果（将要跳转的路由的路径 === '/login(登录页面)'）{
      // if is logged in, redirect to the home page  //如果已登录，则重定向到主页
      next({ path: '/' })  //将要跳转的下一级路由（{路径：'/(主页面)'}）
      NProgress.done() // 跳转进度条.完成（）
    } else { // } 否则 {
      var userInfo = store.state.user.name   // 变量 userInfo(命名) = 储存(管理应用的所有组件的状态).状态.用户.名称
      if (userInfo) { //   如果 （userInfo（存在）{
        turnTo(to, store.state.user.access, next)  // turnTo（将要去的路由，储存.状态.用户权限数组，将要跳转的下一级路由）
      } else {  //   } 否则{  
        console.log('登录成功，已经有token， '但是还没有用户信息，准备获用户信息') 
        //dispatch：含有异步操作，数据提交至 actions ，可用于向后台提交数据
        store.dispatch('user/getInfo').then(user => {  // 储存.dispatch(发出)('user/getInfo').然后（user => {
	//拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问
          turnTo(to, user.access, next)  // turnTo（将要去的路由，用户.用户权限数组，将要跳转的下一级路由）
          console.log(next())
        })                                          
          .catch(() => {  // .接着（（）=>{
            console.log('获取用户信息失败，重置token（令牌）！') 
            store.dispatch('user/resetToken')  //  储存.dispatch(发出)('user/resetTolken')
            next('/login')                                //    将要跳转的下一级路由（‘/login’）
          })                                        
      }                                                   
      // next()
    }                                                 
  }                                                  
  else {
	//没有登录（因为没有令牌）
    /* has no token*/
    if (to.path === '/login') {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login`)
      NProgress.done()
    }
  }
})                                              

//伪代码

// var  hasToken  =  获取令牌()

// if(hasToken){
//   if(已经登录){
//     前往主页
//   }else{
//     var userInfo = 获取用户名()
//     if(userInfo){
//       前往下一级路由
//     }else{
//       获取用户信息().then(()=>{
//         前往下一级路由
//       }).catch(()=>{        
//         刷新令牌信息()
//         回到登录页面
//       })
//     }
//   }
// }else{
//   返回登录页面
// }

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
