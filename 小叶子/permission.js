import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // 设置 page title
  document.title = getPageTitle(to.meta.title)

  // // 判断用户是否登录，登录了是否有用户信息，有用户信息是否有权限访问跳转的路径
  // const hasToken = getToken()//获取token
  // if (hasToken) {//判断是否登录（token）
  //   if (to.path === '/login') {//to.path表示要去的路径
  //     // 登录后，试图重定向到登录页面的，重新跳转到应用首页
  //     next({ path: '/' })//回到首页（roouter中定义）
  //     NProgress.done()
  //   } else {
  //     const hasGetUserInfo = store.getters.name
  //     if (hasGetUserInfo) {//判断是否有用户权限
  //       next()
  //     } else {
  //       try {
  //         // get user info
  //         await store.dispatch('user/getInfo')
  //         next()
  //       } catch (error) {
  //         // remove token and go to login page to re-login
  //         await store.dispatch('user/resetToken')
  //         Message.error(error || 'Has Error')
  //         next(`/login?redirect=${to.path}`)
  //         NProgress.done()
  //       }
  //     }
  //   }
  // } else {
  //   /* 没有登录（has no token）*/

  //   if (whiteList.indexOf(to.path) !== -1) {
  //     // in the free login whitelist, go directly
  //     next()
  //   } else {
  //     //其他没有访问权限的页面是redirec
  //     next(`/login?redirect=${to.path}`)
  //     NProgress.done()
  //   }
  // }

  const hasToken = getToken()                           
  // 常量 hasToken（命名） = 登录后获取的令牌方法
  if (hasToken) {                                       
    // 如果（hasToken）{
    console.log(to.path)
    if (to.path === '/login') {                         
      //   如果（将要去的路由.路径 === '/login(登录页面)'）{
      // if is logged in, redirect to the home page     
      //       //如果已登录，则重定向到主页
      next({ path: '/' })                               
      //       将要跳转的下一级路由（{路径：'/(主页面)'}）
      NProgress.done()                                  
      //       跳转进度条.完成（）
    } else {                                            
      //   } 否则 {
      var userInfo = store.state.user.name              
      //     变量 userInfo(命名) = 储存(管理应用的所有组件的状态).状态.用户.名称
      if (userInfo) {                                   
        //   如果 （userInfo存在）{
        turnTo(to, store.state.user.access, next)       
        //      turnTo（将要去的路由，储存.状态.用户权限数组，将要跳转的下一级路由）
      } else {                                            
        console.log('登录成功，已经有token，',
          '但是还没有用户信息，准备获用户信息')             
          //       输出（‘登录成功，已经有token，但是还没有用户信息，准备获用户信息’）
        //dispatch：含有异步操作，数据提交至 actions ，可用于向后台提交数据
        store.dispatch('user/getInfo').then(user => {   
          //      储存.dispatch(发出)('user/getInfo').然后（user => {
          turnTo(to, user.access, next)                 
          //      turnTo（将要去的路由，用户.用户权限数组，将要跳转的下一级路由）
          console.log(next())
        }).catch(() => {                                
            //        .接着（（）=>{
            console.log('获取 失败！！')  
            //           输出（'获取 失败！！'）
            store.dispatch('user/resetToken')          
            //           储存.dispatch(发出)('user/resetTolken')
            next('/login')                             
            //            将要跳转的下一级路由（‘/login’）
          })                                             
      }                                                  
      // next()
    }                                                    
  }                                                      
  else {
    /* has no token*/

    if (to.path === '/login') {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login`)
      NProgress.done()
    }
  }                                                      

// const hasToken = token（令牌，获取的用户信息）
// 判断（用户信息是否为真）{
//   判断（将要去的路由 全等于 登录路径）{
//     跳转默认主界面
//     精度条结束
//   }否则{
//     var 用户信息 = 通过储存查询用户名
//     判断（用户信息存在）{
//       turnTo(将要去的路由，用户权限数组，默认下一级路由)
//     }否则{
//       去获取用户信息().then((user)=>{
//         turnTo(将要去的路由，user.用户权限数组，默认下一级路由)
//       }).catch(()=>{
//         重置令牌
//         跳转到登录页面
//       })
//     }
//   }
// }否则{
//   判断（将要去的路由 全等于 登录路径 ）{
//     跳转到默认主界面
//   }否则{
//     跳转到（登录路径）
//     进度条结束 
//   }
// }

})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
