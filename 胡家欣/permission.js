import router from './router'
import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import { canTurnTo } from '@/utils/access'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

const turnTo = (to, access, next) => {
  var can = canTurnTo(to.name, access, router.options.routes)
  if (can) next() // 有权限，可访问
  else next({ replace: true, name: 'error_401' }) // 无权限，重定向到401页面
}

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // 设置网页标题
  document.title = getPageTitle(to.meta.title)

  // 判断用户是否登录，登录了是否有用户信息，有用户信息是否有权限访问要跳转的路由
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === '/login') {
      // 登录后，试图重定向到登录页面的，重新跳转到应用首页
      next({ path: '/' })
      NProgress.done()
    } else {
      var userInfo = store.state.user.name
      if (userInfo) {
        turnTo(to, store.state.user.access, next)
      } else {
        console.log('登录成功，已经有token，但是还没有用户信息，准备获用户信息')
        store.dispatch('user/getInfo').then(user => {
          // 拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问;
          // access必须是一个数组，如：['super_admin'] ['super_admin', 'admin']
          turnTo(to, user.access, next)
        }).catch(() => {
          // 获取用户信息失败，重置token
          store.dispatch('user/resetToken')
          next('/login')
        })
      }
    }
  } else {
    // 没有登录（因为没有token）

    if (to.path === '/login') {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})


// 伪代码
// const hasToken = token（获取的用户信息）
// 判断（用户信息是否正确）{
//   判断（将要去的路由 全等于 登录路径）
//   是{
//     进度条结束
//     跳转默认主界面
//   }否{
//     var 用户信息 = 通过储存查询用户名
//     判断（用户信息存在）
//     是{
//       turnTo(将要去的路由，用户权限数组，默认下一级路由)
//     }否{
//       去获取用户信息().then((user)=>{
//         turnTo(将要去的路由，user.用户权限数组，默认下一级路由)
//       }).catch(()=>{
//         重置令牌
//         跳转登录页面
//         })
//       }
// var  hasToken  =  获取令牌()
//
//
// if(hasToken){
//   if(已经登录){
//     前往主页
//   }else{
//     var userInfo = 获取用户名()
//     if(userInfo){
//       前往下一级路由
//     }else{
//       获取用户信息().then(()=>{
//         前往下一级路由
//       }).catch(()=>{        
//         刷新令牌信息()
//         回到登录页面
//       })
//     }
//   }
// }否则{
//   判断（将要去的路由 全等于 登录路径 ）{
//     跳转到默认主界面
//   }否则{
//     跳转到（登录路径）
//     进度条结束 
//   }
// }else{
//   返回登录页面
// }

